function toggleSwitch(event) {
    let texts = event.closest(`.switch`).querySelectorAll(`.switch-text`);
    texts.forEach((texts) => {
        texts.classList.remove(`active`);
    });
    texts[Number(event.checked)].classList.add(`active`);
}
