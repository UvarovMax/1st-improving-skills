function toggleOptions(event) {
  let parentContainer = event.closest(`.custom-select`);
  let options = parentContainer.querySelector(`.custom-select__options`);
  options.style.display = options.style.display === "block" ? "none" : "block";
}

function selectOption(option) {
  let parentContainer = option.closest(`.custom-select`);
  const options = parentContainer.querySelectorAll(`.custom-select__option`);
  options.forEach((option) => {
    option.classList.remove("selected");
  });

  parentContainer.querySelector(`.custom-select__selected-text`).innerText = option.innerText;
  option.classList.add("selected");

  toggleOptions(option);
}
