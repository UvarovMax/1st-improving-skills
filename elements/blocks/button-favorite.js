function changeFavoriteStatus(event) {
    let fav = event.dataset.fav;
    let img = event.querySelector(`img`);

    if (fav === "true") {
        img.src = `./res/svg/heart-solid.svg`;
        fav = "false";
    } else {
        img.src = `./res/svg/heart-solid-active.svg`;
        fav = "true";
    }
    event.dataset.fav = fav;
}
